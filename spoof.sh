#! /usr/bin/bash
### Script for generating signature spoofing zip for android 11 ###

echo "Unzipping haystack......"
sleep 1
unzip haystack.zip

cp dexpatcher.jar haystack-11-attempt/
cd haystack-11-attempt/
mv spoof_AVDapi30.zip.ONLY\'MAGISK\&ANDROID-STUDIO spoof_AVDapi30.zip
if [ $(adb devices | wc -l) -gt 2 ]
then
    echo "Device found. Extracting services.jar ........."
    adb pull /system/framework/services.jar
else
    echo "No Device found. Exiting."
    cd .. && rm -rf haystack-11-attempt/
    sleep 2
    exit
fi
java -jar dexpatcher.jar -a 11 -M -v -d -o ./ services.jar 11-hook-services.jar.dex 11core-services.jar.dex
mkdir system system/framework/
zip -j system/framework/services.jar classes*.dex
zip spoof_AVDapi30.zip system/framework/services.jar
mv spoof_AVDapi30.zip ../
cd ../
rm -rf haystack-11-attempt/

# Push to phone
echo -n "Do you like to push to your phone's storage?(y/n):"
read flag
if [ $flag == "y" ] || [ $flag == "Y" ]
then
    echo "Pushing to phone's storage......."
    adb push spoof_AVDapi30.zip /sdcard/
    echo "Done."
else
    echo "Done."
fi