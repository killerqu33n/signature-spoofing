# Signature Spoofing

This script will automate generation of signature spoofing zip. Follow the below instructions.

---

## Requirements
Custom ROM(LineageOS)\
Magisk

---

## Dependencies

* zip
* unzip
* java
* adb
---

## Instructions

Enable USB debugging and root-adb-debugging on Developer options.\
Clone the repo.\
Make spoof.sh executable.\
Run spoof.sh

---
